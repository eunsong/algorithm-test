const router = require('express').Router();


router.post('/test001', (req, res) => {

    let samples = req.body;

    const solution = (str) => {
        var answer = true;
        if(str.length !== 4 && str.length !== 6){
            answer = false;
        }else{
            if(isNaN(str)){ answer = false; }
        }
        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.input);
        return testcase;
    });

    res.send(result);
});

router.post('/test002', (req, res) => {

    let samples = req.body;

    const solution = (str) => {
        var answer = str.toString().split("").reverse().map(num => num = parseInt(num));
        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.input);
        return testcase;
    });

    res.send(result);
});

router.post('/test003', (req, res) => {

    let samples = req.body;

    const solution = (m, tree) => {
        var answer = "";
        var maxHeight = tree[tree.length - 1];

        var start = 0;
        var end = maxHeight;
        while(start <= end){
            var sum = 0;
            answer = Math.floor((start + end) / 2);
            for(var idx in tree){
                if(answer < tree[idx]){ sum += tree[idx] - answer; }
            }
            if(sum < m){
                end = answer;
            }else if(sum > m){
                start = answer + 1;
            }else{
                break;
            }
        }
        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.m, testcase.tree.sort());
        return testcase;
    });

    res.send(result);
});

router.post('/test004', (req, res) => {

    let samples = req.body;

    const solution = (n, budget, m) => {
        var answer = 0;
        var totalBudget = 0;
        var left = 0;
        var right = budget.sort()[n -1];
        
        for(var idx in budget){
            totalBudget += budget[idx];
        }

        if(totalBudget <= m) return right;

        while(left <= right){
            var sum = 0;
            var midBudget = Math.floor((left + right) / 2);
            for(var budgetIdx in budget){
                midBudget < budget[budgetIdx] ? sum += midBudget : sum += budget[budgetIdx];
            }

            if(sum > m){
                left = midBudget + 1;
            }else if(sum < m){
                right = midBudget - 1;
            }else{
                break;
            }
        }

        return midBudget;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.n, testcase.budget, testcase.m);
        return testcase;
    });

    res.send(result);
});

router.post('/test005', (req, res) => {
    
    let samples = req.body;

    const solution = (n, times) => {
        var answer = 0;
        var maxTime = 0;
        for(var idx in times){
            if(maxTime <= times[idx]){
                maxTime = times[idx];
            }
        }
        var left = 0;
        var right = maxTime * n;

        while(left <= right){
            answer = Math.floor((left + right) / 2);
            
            var completeJob = 0;
            for(var timeIdx in times){
                completeJob += Math.floor(answer / times[timeIdx]);
            }

            if(completeJob > n){
                right = answer - 1;
            }else if(completeJob < n){
                left = answer + 1;
            }else{
                break;
            }
        }
        return answer;
    };

    const result = samples.map( (testcase) => {
        testcase.output = solution(testcase.n, testcase.times);
        return testcase;
    });

    res.send(result);
});

module.exports = router;